﻿using FoundationAutomationExamination.Common;
using OpenQA.Selenium;

namespace FoundationAutomationExamination.PageObjects
{
    class CreateAccountPage : BasePage
    {
        private By mrsRadioButton = By.Id("uniform-id_gender2");
        private By customerFirstNameTextbox = By.Id("customer_firstname");
        private By customerLastNameTextbox = By.Id("customer_lastname");
        private By passwordTextbox = By.Id("passwd");

        private By birthDaySelection = By.Id("days");
        private By birthMonthSelection = By.Id("months");
        private By birthYearSelection = By.Id("years");

        private By selectedDay(int day) => By.XPath($"//select[contains(@id,'days')] //option[@value='{day}']");
        private By selectedMonth(int month) => By.XPath($"//select[contains(@id,'months')] //option[@value='{month}']");
        private By selectedYear(int year) => By.XPath($"//select[contains(@id,'years')] //option[@value='{year}']");

        private By companyTextbox = By.Id("company");
        private By addressTextbox = By.Id("address1");
        private By cityTextbox = By.Id("city");
        private By stateDropdownList = By.Id("id_state");
        private By stateSelected = By.CssSelector("#id_state option:nth-child(3)");
        private By postcodeTextbox = By.Id("postcode");
        private By mobilePhoneTextBox = By.Id("phone_mobile");
        private By submitAccount = By.Id("submitAccount");
        
        public CreateAccountPage(IWebDriver driver) : base(driver)
        {

        }

        public MyAccountPage InputPersonalInformation(string firstName, string lastName)
        {
            WaitForElementVisible(mrsRadioButton);
            ClickToElement(mrsRadioButton);
            SendKeyToElement(customerFirstNameTextbox, firstName);
            SendKeyToElement(customerLastNameTextbox, lastName);
            SendKeyToElement(passwordTextbox, "Demo@123");
            SendKeyToElement(passwordTextbox, "Demo@123");
            SendKeyToElement(companyTextbox, "Demo");
            SendKeyToElement(addressTextbox, "123 ABC, CDF");
            SendKeyToElement(cityTextbox, "NY");

            ClickToElement(birthDaySelection);
            ClickToElement(selectedDay(6));

            ClickToElement(birthMonthSelection);
            ClickToElement(selectedMonth(5));

            ClickToElement(birthYearSelection);
            ClickToElement(selectedYear(1993));

            ClickToElement(stateDropdownList);
            WaitForElementVisible(stateSelected);
            ClickToElement(stateSelected);

            SendKeyToElement(postcodeTextbox, "00000");
            SendKeyToElement(mobilePhoneTextBox, "0701234567");

            ClickToElement(submitAccount);

            return new MyAccountPage(driver);
        }
    }
}
