﻿using FoundationAutomationExamination.Common;
using OpenQA.Selenium;

namespace FoundationAutomationExamination.PageObjects
{
    class MyAccountPage : BasePage
    {
        private By accountLabel = By.ClassName("account");

        public MyAccountPage(IWebDriver driver) : base(driver)
        {
        }

        public string GetAccountLabel()
        {
            WaitForElementVisible(accountLabel);
            IWebElement label = FindElement(accountLabel);
            return label.Text;
        }
    }
}
