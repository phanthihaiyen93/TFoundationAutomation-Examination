﻿using FoundationAutomationExamination.Common;
using OpenQA.Selenium;

namespace FoundationAutomationExamination.PageObjects
{
    class AuthenticationPage: BasePage
    {
        private By emailTextbox = By.Id("email_create");
        private By createAccountButton = By.Id("SubmitCreate");


        public AuthenticationPage(IWebDriver driver) : base(driver)
        {

        }

        public CreateAccountPage InputEmail(string email)
        {
            SendKeyToElement(emailTextbox, email);
            ClickToElement(createAccountButton);
            return new CreateAccountPage(driver);
        }
    }
}
