﻿using FoundationAutomationExamination.Common;
using FoundationAutomationExamination.DataObjects;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FoundationAutomationExamination.PageObjects
{
    class HomePage : BasePage
    {
        private By signInButton = By.ClassName("login");
        private By searchTextBox = By.Id("search_query_top");
        private By searchButton = By.CssSelector("button[name='submit_search']");
        private By productNames = By.CssSelector(".right-block .product-name");
        private By productPrices = By.CssSelector(".right-block .product-price");
        private By categoryLink(string Category) => By.CssSelector($"a[title='{Category}']");
        private By subCategoryLink(string subCategory) => By.CssSelector($".submenu-container a[title='{subCategory}']");

        public HomePage(IWebDriver driver) : base(driver)
        {
        }

        public AuthenticationPage GoToAuthenticationPage()
        {
            ClickToElement(signInButton);
            return new AuthenticationPage(driver);
        }

        public void ClickOnSubMenu(string category, string subCategory)
        {
            IWebElement element = FindElement(categoryLink(category));
            Actions action = new Actions(driver);
            action.MoveToElement(element).Perform();

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
            var TShirtLinkElement = wait.Until(ExpectedConditions.ElementIsVisible(subCategoryLink(subCategory)));
            TShirtLinkElement.Click();
        }

        public Product GetFirstProductOnPage()
        {
            var names = this.FindElements(productNames);
            if (names != null && names.Count > 0)
            {
                var prices = this.FindElements(productPrices);

                return new Product
                {
                    ProductName = names.First().Text,
                    Price = prices.First().Text
                };
            }
            else
            {
                return null;
            }            
        }

        public IList<IWebElement> SearchProduct(string product)
        {
            SendKeyToElement(searchTextBox, product);
            ClickToElement(searchButton);
            return this.FindElements(productNames);
        }
    }
}
