using FoundationAutomationExamination.Common;
using FoundationAutomationExamination.PageObjects;
using NUnit.Framework;
using OpenQA.Selenium;
using System;

namespace CreateAccountTests
{
    class CreateAccountTests: WebDriverManagers
    {
        IWebDriver driver;
        HomePage homePage;
        AuthenticationPage authenticationPage;
        CreateAccountPage createAccountPage;
        MyAccountPage myAccountPage;

        [SetUp]
        public void Setup()
        {
            driver = CreateBrowserDriver("edge");
            driver.Navigate().GoToUrl(Constant.APP_URL);
        }

        [Test]
        public void CreateAccountTest()
        {
            homePage = new HomePage(driver);
            authenticationPage = homePage.GoToAuthenticationPage();

            var email = string.Format("yp{0}@gmail.com", Guid.NewGuid());
            createAccountPage = authenticationPage.InputEmail(email);
            myAccountPage = createAccountPage.InputPersonalInformation("Yoki", "Phan");

            var accountName = myAccountPage.GetAccountLabel();
            Assert.AreEqual("Yoki Phan", accountName, "Create an account not successfully!");
        }

        [TearDown]
        public void TearDown()
        {
            driver.Quit();
        }
    }
}