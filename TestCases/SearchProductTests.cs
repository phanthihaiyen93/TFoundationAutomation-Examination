using FoundationAutomationExamination.Common;
using FoundationAutomationExamination.PageObjects;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;

namespace SearchProductTests
{
    class SearchProductTests : WebDriverManagers
    {
        IWebDriver driver;
        HomePage homePage;

        [SetUp]
        public void Setup()
        {
            driver = CreateBrowserDriver("edge");
            driver.Navigate().GoToUrl(Constant.APP_URL);
        }

        [Test]
        public void SearchProductTest()
        {
            homePage = new HomePage(driver);

            homePage.ClickOnSubMenu("Women", "T-shirts");
            var product = homePage.GetFirstProductOnPage();

            IList <IWebElement> products = homePage.SearchProduct(product?.ProductName);

            Assert.IsNotNull(products);
            Assert.IsNotEmpty(products);
            Assert.AreEqual(product.ProductName, products.First().Text, "Search result is not correct");
        }

        [TearDown]
        public void TearDown()
        {
            driver.Quit();
        }
    }
}